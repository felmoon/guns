package com.memotech.memo.generator.action;


import com.memotech.memo.generator.action.config.MemoGeneratorConfig;

/**
 * 代码生成器,可以生成实体,dao,service,controller,html,js
 *
 * @author memotech
 * @Date 2017/5/21 12:38
 */
public class MemoCodeGenerator {

    public static void main(String[] args) {

        /**
         * Mybatis-Plus的代码生成器:
         *      mp的代码生成器可以生成实体,mapper,mapper对应的xml,service
         */
        MemoGeneratorConfig MemoGeneratorConfig = new MemoGeneratorConfig();
        MemoGeneratorConfig.doMpGeneration();

        /**
         * memo的生成器:
         *      memo的代码生成器可以生成controller,html页面,页面对应的js
         */
        MemoGeneratorConfig.domemoGeneration();
    }

}