package com.memotech.memo.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.memotech.memo"})
public class MemoRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemoRestApplication.class, args);
    }
}
