package com.memotech.memo.rest.common.persistence.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.memotech.memo.rest.common.persistence.model.User;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author memotech
 * @since 2017-08-23
 */
public interface UserMapper extends BaseMapper<User> {

}