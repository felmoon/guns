package com.memotech.memo.core.util;

import com.memotech.memo.config.properties.MemoProperties;

/**
 * 验证码工具类
 */
public class KaptchaUtil {

    /**
     * 获取验证码开关
     *
     * @author memotech
     * @Date 2017/5/23 22:34
     */
    public static Boolean getKaptchaOnOff() {
        return SpringContextHolder.getBean(MemoProperties.class).getKaptchaOpen();
    }
}