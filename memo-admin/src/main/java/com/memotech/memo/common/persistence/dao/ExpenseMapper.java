package com.memotech.memo.common.persistence.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.memotech.memo.common.persistence.model.Expense;

/**
 * <p>
  * 报销表 Mapper 接口
 * </p>
 *
 * @author memotech
 * @since 2017-12-04
 */
public interface ExpenseMapper extends BaseMapper<Expense> {

}