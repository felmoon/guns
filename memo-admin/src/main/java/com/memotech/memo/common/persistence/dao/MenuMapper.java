package com.memotech.memo.common.persistence.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.memotech.memo.common.persistence.model.Menu;

/**
 * <p>
  * 菜单表 Mapper 接口
 * </p>
 *
 * @author memotech
 * @since 2017-07-11
 */
public interface MenuMapper extends BaseMapper<Menu> {

}