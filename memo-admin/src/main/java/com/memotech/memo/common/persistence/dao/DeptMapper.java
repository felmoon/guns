package com.memotech.memo.common.persistence.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.memotech.memo.common.persistence.model.Dept;

/**
 * <p>
  * 部门表 Mapper 接口
 * </p>
 *
 * @author memotech
 * @since 2017-07-11
 */
public interface DeptMapper extends BaseMapper<Dept> {

}