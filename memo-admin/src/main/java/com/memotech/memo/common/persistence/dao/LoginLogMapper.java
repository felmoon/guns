package com.memotech.memo.common.persistence.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.memotech.memo.common.persistence.model.LoginLog;

/**
 * <p>
  * 登录记录 Mapper 接口
 * </p>
 *
 * @author memotech
 * @since 2017-07-11
 */
public interface LoginLogMapper extends BaseMapper<LoginLog> {

}